const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser =  require('body-parser');
const mongoose = require('mongoose');
const config = require('./configuration/config');
const io = require('socket.io');
//const oauthserver = require('oauth2-server');

const userRoutes = require('./routes/users');
const indexRoutes = require('./routes/index');


//Mongo Database Connection
mongoose.connect(`mongodb://${config.mongodb.username}:${config.mongodb.password}@${config.web.hostname}/${config.mongodb.db_name}`, {useNewUrlParser: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('DB connected successfully');
});
//OAuth2 Server
/*
app.oauth = oauthserver({
    model: models.oauth,
    grants: ['password', 'authorization_code', 'refresh_token'],
    debug: true
  });

app.all('/oauth/token', app.oauth.grant());
app.get('/', app.oauth.authorise(), function (req, res) {
    res.send('Secret area');
});
app.use(app.oauth.errorHandler());
*/
app.use(bodyParser.urlencoded({extended: false})); //false in js object, because it has simple body
app.use(bodyParser.json());
app.use(morgan('dev'));

app.get('/socket', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

//Routes that handle requests-Middlewares
app.use('/', indexRoutes);
app.use('/users', userRoutes);

//ERROR handling
app.use((req, res, next) => {
    const error =  new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error:{
            message: error.message
        }
    });
});

module.exports = app;