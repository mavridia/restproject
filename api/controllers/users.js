const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = require('../models/users');

exports.getAllusers =  (req, res, next) => {
    User.find()
    .exec()
    .then(docs =>{
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.createUser = (req, res, next) => {
    //Use it as a constructor of the schema
    const user = new User({
        _id: new mongoose.Types.ObjectId,
        username: req.body.username,
        email: req.body.email
    });
    //User saved in mongodb named Users
    user.save().then(result => {
        res.status(200).json({
            message: 'Handling POST message',
            createduser: result
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
}


exports.getUserByID = (req, res, next) => {
    const id = req.params.userID;
    User.findById(id)
        .exec()
        .then( doc =>{
            if(doc){
                res.status(200).json(doc);
            }else{
                res.status(404).json({message: "No valid entry found"});
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.updateUser = (req, res, next) => {
    const id = req.params.userID;
    User.findById(id)
        .exec()
        .then( doc =>{
            const updateOps = {};               //array for the option to be updates
            for(const ops in req.body){
                updateOps[ops] = req.body[ops];
            }
            User.update({_id: id}, {username: req.body.username, email: req.body.email})
                .exec()
                .then(result =>{
                    res.status(200).json(result);
                })
                .catch(err =>{
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.deleteUser = (req, res, next) => {
    const id = req.params.userID;
    User.remove({_id: id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err =>{
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.signup = (req, res, next) => {
    console.log('UserController.signup called');
}

exports.signin = (req, res, next) => {
    console.log('UserController.signin called');
}

/*exports.secret = (req, res, next) => {
    console.log('UserController.secret called');
}*/