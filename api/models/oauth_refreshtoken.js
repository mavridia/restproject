const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OAuthRefreshTokensSchema = new Schema({
    refreshToken: { type: String, required: true, unique: true },
    clientId: String,
    userId: { type: String, required: true },
    expires: Date
});

mongoose.model('oauth_refreshtokens', OAuthRefreshTokensSchema);

const OAuthRefreshTokensModel = mongoose.model('oauth_refreshtokens');

module.exports.saveRefreshToken = function(token, clientId, expires, userId, callback) {
    if (userId.id) {
        userId = userId.id;
    }

    var refreshToken = new OAuthRefreshTokensModel({
        refreshToken: token,
        clientId: clientId,
        userId: userId,
        expires: expires
    });

    refreshToken.save(callback);
};

module.exports.getRefreshToken = function(refreshToken, callback) {
    OAuthRefreshTokensModel.findOne({ refreshToken: refreshToken }, function(err, token) {
    if (token) {
        token.user = token.userId;
    }
        callback(err, token);
    });
};