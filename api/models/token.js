const mongoose = require('mongoose');

const tokenSchema =  mongoose.Schema({
    type: String,
    unique: true,
    required: true
});

const AccessTokenModel = mongoose.model('AccessToken', AccessToken);
module.exports.AccessTokenModel = AccessTokenModel;