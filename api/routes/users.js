const express = require('express');
const router = express.Router();
const bodyParser =  require('body-parser');
const mongoose = require('mongoose');

const User = require('../models/users');
const UserController = require('../controllers/users');

//Handling incoming GET/POST/PATCH/DELETE requests

router.route('/')
    .get(UserController.getAllusers);

router.route('/')
    .post(UserController.createUser);

router.route('/:userID')
    .get(UserController.getUserByID)
    .patch(UserController.updateUser)
    .delete(UserController.deleteUser);

router.route('/signup')
    .post(UserController.signup);

router.route('/signin')
    .post(UserController.signin);

/*router.route('/secret')
    .get(UserController.secret);*/

module.exports = router;