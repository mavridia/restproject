const http = require('http');
const express = require('express');
const app = require('./app');

const config = require('./configuration/config');

const port = process.env.PORT || 3000;
const hostname = process.env.hostname || config.web.hostname;
const server = http.createServer(app);

// Setup socket.io
var io = require('socket.io').listen(server);

// on new connection
io.on('connection', function (client) {
	console.log('Client connected...');

	client.on('join', function (data) {
		console.log(data);

		// wait some seconds and then respond back to client
		setTimeout(() => {
			client.emit('messages', 'Hello from server');
		}, 2000);
	});
});

server.listen(port, hostname);